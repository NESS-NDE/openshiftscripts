# OpenShiftScripts


Here are the steps to export and import labeled resources.

export resources
$ oc login <source_environment>
$ oc project <source_project>
$ oc export dc,is,svc,route,secret,sa -l <label=value> -o yaml > export.yaml

import resources
$ oc login <target_environment>
$ oc new-project <target_project> 
$ oc create -f export.yaml

e.g. label app=dev-python36-app

oc export dc,is,svc,route,secret,sa -l app=dev-python36-app  -o yaml > dev-python36-app.yaml

